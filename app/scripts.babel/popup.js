(function () {
  'use strict'
  var jq = window.jQuery
  var $input = jq('#input')
  var $code = jq('#qrcode')
  // update qrcode
  function update () {
    var val = $input[0].value
    $code.empty()
    $code.qrcode({
      width: 300,
      height: 300,
      text: val
    })
  }
  // input
  $input.keyup(function () {
    update()
  })
  // current tab url
  chrome.tabs.query({ active: true, currentWindow: true }, tabs => {
    if (tabs && tabs.length > 0) {
      $input[0].value = tabs[0].url
      update()
    }
  })
}())
