# qrcode for chrome

Generate a QR Code.

![screenshot](screenshot.png)

## Install

[![webstore](https://developer.chrome.com/webstore/images/ChromeWebStore_BadgeWBorder_v2_206x58.png)](https://chrome.google.com/webstore/detail/cgabjpdamljhnijpoedehljamiidpkcl)

## License

MIT, see more [LICENSE](LICENSE)
